package org.achille.tpXpathExo4;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class Consultation {

	private static List<String> liste=new  ArrayList<String>();

	public static List<String> getListe() {
		return liste;
	}

	public static void setListe(List<String> liste) {
		Consultation.liste = liste;
	}
	
	
	public static List<Element> valeurAttributName() throws DocumentException{
		
		File f= new File("files/AWSECommerceService.wsdl.xml");
		SAXReader sax=new SAXReader ();
		Document doc=sax.read(f);
		@SuppressWarnings("unused")
		Element rootElemnt=doc.getRootElement();
		@SuppressWarnings("unchecked")
		List<Element> elements= doc.selectNodes("//*[local-name()='types']/xs:schema/*");
		for(int i=0;i<elements.size();i++)
		{
			elements.get(i).attributeValue("name");
		}
		return elements;
	}

	public static List<Element> valeurAttributNameMessage() throws DocumentException
	{
		File f= new File("files/AWSECommerceService.wsdl.xml");
		SAXReader sax=new SAXReader ();
		Document doc=sax.read(f);
		@SuppressWarnings("unused")
		Element rootElemnt=doc.getRootElement();
		@SuppressWarnings("unchecked")
		List<Element> elements= doc.selectNodes("//*[name()='message']");
		for(int i=0;i<elements.size();i++)
		{
			elements.get(i).attributeValue("name");
		}
		return elements;
	}


}
