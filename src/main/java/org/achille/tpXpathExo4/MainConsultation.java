package org.achille.tpXpathExo4;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;

public class MainConsultation {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, DocumentException {
				
		File f= new File("files/AWSECommerceService.wsdl.xml");
		
		SAXReader sax=new SAXReader();

		Document doc=sax.read(f);
		
		@SuppressWarnings("unused")
		Element rootElemnt=doc.getRootElement();
		
		@SuppressWarnings("unchecked")
		List<Element> elements= doc.selectNodes("//*[local-name()='types']/xs:schema/*");		
		
		System.out.println("\n4a/ \n Nombre De sous-élément dans types/schema :"+elements.size() + "\n");
		
		System.out.println("4b/Liste des valeurs des attributs names shema sont : ");
		for(Element valeur: Consultation.valeurAttributName()){
			System.out.println(valeur.attributeValue("name"));
		}

		System.out.println(("\n"));
        
		System.out.println("\n5/Liste des valeurs des attributs names message : ");
		for(Element s:Consultation.valeurAttributNameMessage()){
			System.out.println(s.attributeValue("name"));
		}

	}
}
